'From Cuis 5.0 [latest update: #4112] on 3 May 2020 at 6:24:18 pm'!
'Description '!
!provides: 'Zapp' 1 0!
SystemOrganization addCategory: #Zapp!


!classDefinition: #Directory category: #Zapp!
Object subclass: #Directory
	instanceVariableNames: 'path contents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zapp'!
!classDefinition: 'Directory class' category: #Zapp!
Directory class
	instanceVariableNames: ''!

!classDefinition: #Backend category: #Zapp!
Directory subclass: #Backend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zapp'!
!classDefinition: 'Backend class' category: #Zapp!
Backend class
	instanceVariableNames: ''!

!classDefinition: #Frontend category: #Zapp!
Directory subclass: #Frontend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zapp'!
!classDefinition: 'Frontend class' category: #Zapp!
Frontend class
	instanceVariableNames: ''!

!classDefinition: #File category: #Zapp!
Object subclass: #File
	instanceVariableNames: 'name contents noExtension extension'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zapp'!
!classDefinition: 'File class' category: #Zapp!
File class
	instanceVariableNames: ''!

!classDefinition: #JSONFile category: #Zapp!
File subclass: #JSONFile
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zapp'!
!classDefinition: 'JSONFile class' category: #Zapp!
JSONFile class
	instanceVariableNames: ''!

!classDefinition: #JavaScriptFile category: #Zapp!
File subclass: #JavaScriptFile
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zapp'!
!classDefinition: 'JavaScriptFile class' category: #Zapp!
JavaScriptFile class
	instanceVariableNames: ''!

!classDefinition: #TextFile category: #Zapp!
File subclass: #TextFile
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zapp'!
!classDefinition: 'TextFile class' category: #Zapp!
TextFile class
	instanceVariableNames: ''!

!classDefinition: #Project category: #Zapp!
Object subclass: #Project
	instanceVariableNames: 'frontend backend config path'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zapp'!
!classDefinition: 'Project class' category: #Zapp!
Project class
	instanceVariableNames: ''!

!classDefinition: #ProjectConfig category: #Zapp!
Object subclass: #ProjectConfig
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zapp'!
!classDefinition: 'ProjectConfig class' category: #Zapp!
ProjectConfig class
	instanceVariableNames: ''!

!classDefinition: #ZLanguageStructure category: #Zapp!
Object subclass: #ZLanguageStructure
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zapp'!
!classDefinition: 'ZLanguageStructure class' category: #Zapp!
ZLanguageStructure class
	instanceVariableNames: ''!

!classDefinition: #JSLangStruct category: #Zapp!
ZLanguageStructure subclass: #JSLangStruct
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zapp'!
!classDefinition: 'JSLangStruct class' category: #Zapp!
JSLangStruct class
	instanceVariableNames: ''!

!classDefinition: #Assignment category: #Zapp!
JSLangStruct subclass: #Assignment
	instanceVariableNames: 'LHS RHS'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zapp'!
!classDefinition: 'Assignment class' category: #Zapp!
Assignment class
	instanceVariableNames: ''!

!classDefinition: #TSLangStruct category: #Zapp!
JSLangStruct subclass: #TSLangStruct
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zapp'!
!classDefinition: 'TSLangStruct class' category: #Zapp!
TSLangStruct class
	instanceVariableNames: ''!

!classDefinition: #UseStrict category: #Zapp!
JSLangStruct subclass: #UseStrict
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zapp'!
!classDefinition: 'UseStrict class' category: #Zapp!
UseStrict class
	instanceVariableNames: ''!

!classDefinition: #ZModule category: #Zapp!
Object subclass: #ZModule
	instanceVariableNames: 'name'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zapp'!
!classDefinition: 'ZModule class' category: #Zapp!
ZModule class
	instanceVariableNames: ''!

!classDefinition: #AdonisModule category: #Zapp!
ZModule subclass: #AdonisModule
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zapp'!
!classDefinition: 'AdonisModule class' category: #Zapp!
AdonisModule class
	instanceVariableNames: ''!


!ZModule methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 16:24:38'!
name
	"comment stating purpose of message"

	^ name.! !

!Directory methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 15:19:38'!
add: anObject
	"comment stating purpose of message"

	contents add: anObject.! !

!Directory methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 15:19:02'!
initialize
	"comment stating purpose of message"

	contents := Bag new.! !

!Directory methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 11:48:03'!
path
	"comment stating purpose of message"

	^ path.! !

!Directory methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 10:56:38'!
withPath: dirPath
	"comment stating purpose of message"

	path := dirPath.
	^ self.! !

!Directory methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 16:08:30'!
write
	"comment stating purpose of message"
	| acc |
	acc := FileIOAccessor new.
	(acc basicDirectoryExists: path)
		ifFalse: [ acc createDirectory: path ].
	contents do: [ :x | x writeTo: path ].! !

!Directory methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 16:14:45'!
writeTo: aPath
	"comment stating purpose of message"
	| acc thePath |
	acc := FileIOAccessor new.
	thePath := aPath, path.
	(acc basicDirectoryExists: thePath)
		ifFalse: [ acc createDirectory: thePath ].
	contents do: [ :x | x writeTo: thePath ].! !

!File methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 17:36:46'!
getContents
	"comment stating purpose of message"

	^ contents.! !

!File methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 17:44:53'!
initialize
	"comment stating purpose of message"
	name := 'unnamed'.! !

!File methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 17:10:03'!
named: aString
	"comment stating purpose of message"

	name := aString.! !

!File methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 16:31:07'!
named: nameString withExtension: extString
	"comment stating purpose of message"

	name := nameString.
	extension := extString.! !

!File methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 15:56:21'!
newline
	"comment stating purpose of message"

	^ '
'.! !

!File methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 17:44:32'!
noExtension
	"comment stating purpose of message"

	extension := nil.! !

!File methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 10:36:37'!
subclassResponsibility
	"comment stating purpose of message"
	self error: 'My subclass should have overridden ', thisContext sender selector printString.! !

!File methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 17:43:51'!
writeTo: parentPath
	"comment stating purpose of message"
	| fileName |
	fileName := name.
	extension = nil ifFalse: [ fileName := fileName, '.', extension ].
	^ (parentPath, '/', fileName) asFileEntry fileContents: (self getContents).! !

!JSONFile methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 16:41:06'!
initialize
	"comment stating purpose of message"

	extension := 'json'.! !

!JavaScriptFile methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 17:26:47'!
asString
	"comment stating purpose of message"
	| result |
	result := ''.
	contents do: [ :x | result := result, (x asString) ].
	^ result.! !

!JavaScriptFile methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 17:56:16'!
const
	"comment stating purpose of message"

	^ Assignment new.! !

!JavaScriptFile methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 17:37:22'!
getContents
	"comment stating purpose of message"

	^ self asString.! !

!JavaScriptFile methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 16:51:57'!
initialize
	"comment stating purpose of message"

	extension := 'js'.
	contents := Bag new.! !

!JavaScriptFile methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 17:24:15'!
useStrict
	"comment stating purpose of message"

	contents add: UseStrict new.! !

!TextFile methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 15:52:43'!
append: aString
	"comment stating purpose of message"

	contents := contents, aString.! !

!TextFile methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 15:55:53'!
appendLine: aString
	"comment stating purpose of message"

	self append: (aString, (self newline)).! !

!TextFile methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 16:38:38'!
initialize
	"comment stating purpose of message"

	extension := 'txt'.
	contents := ''.! !

!Project methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 12:19:44'!
backend
	"comment stating purpose of message"

	^ backend.! !

!Project methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 11:50:29'!
frontend
	"comment stating purpose of message"

	^ frontend.! !

!Project methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 12:23:03'!
initialize
	"comment stating purpose of message"

	path = nil
		ifTrue: [ path := '/home/morgan/workspace/zapp-script/test/cuis' ].
	self withPath: path.! !

!Project methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 16:25:18'!
withModule: aModule
	"comment stating purpose of message"

	aModule withProject: self.! !

!Project methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 12:22:46'!
withPath: aPath
	"comment stating purpose of message"

	path := aPath.
	backend := Backend new withPath: path.
	frontend := Frontend new withPath: (path, '/resources').
	config := ProjectConfig new.! !

!Project methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 12:05:29'!
write
	"comment stating purpose of message"
	
	backend write.
	frontend write.! !

!ZLanguageStructure methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 16:59:47'!
asString
	"comment stating purpose of message"

	self subclassResponsibility.! !

!ZLanguageStructure methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 17:27:45'!
newline
	"comment stating purpose of message"

	^ '
'.! !

!ZLanguageStructure methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 16:59:20'!
subclassResponsibility
	"comment stating purpose of message"
	self error: 'My subclass should have overridden ', thisContext sender selector printString.! !

!UseStrict methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 17:27:58'!
asString
	"comment stating purpose of message"

	^ '''use strict''', ';', (self newline).! !

!AdonisModule methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 16:23:54'!
initialize
	"comment stating purpose of message"

	name := 'adonis'.! !

!AdonisModule methodsFor: 'as yet unclassified' stamp: 'MCM 5/3/2020 16:25:57'!
withProject: aProject
	"comment stating purpose of message"
! !
